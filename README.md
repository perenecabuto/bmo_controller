
BMO CONTROLLER
==============

About
-----

Interface para controlador universal de sinais de radio e infravermelho.

How does it works
-----------------

* Scaneia sinais de diversas origens (rf315, rf433, infrared).
* Clona e replica estes sinais.
* Suporta listeners para tomar acoes quando algum sinal for detectado, executando um comando no server ou chamando um sinal clonado.

