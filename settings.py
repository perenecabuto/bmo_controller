# -*- coding: utf-8 -*-

DEBUG = True

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': 'bmo_controller.sqlite3',
    }
}

SECRET_KEY = 'DUMMY'

ROOT_URLCONF = 'urls'

STATIC_ROOT = 'static/'
STATIC_URL = '/static/'

INSTALLED_APPS = (
    'django.contrib.staticfiles',

    'bmo_controller',
    'django_extensions',
)

#if DEBUG:
    #INSTALLED_APPS += ('debug_toolbar',)

    #MIDDLEWARE_CLASSES = (
        #'debug_toolbar.middleware.DebugToolbarMiddleware',
    #)

    #INTERNAL_IPS = ('127.0.0.1',)

    #DEBUG_TOOLBAR_PANELS = (
        #'debug_toolbar.panels.version.VersionDebugPanel',
        #'debug_toolbar.panels.timer.TimerDebugPanel',
        #'debug_toolbar.panels.settings_vars.SettingsVarsDebugPanel',
        #'debug_toolbar.panels.headers.HeaderDebugPanel',
        #'debug_toolbar.panels.request_vars.RequestVarsDebugPanel',
        #'debug_toolbar.panels.template.TemplateDebugPanel',
        #'debug_toolbar.panels.sql.SQLDebugPanel',
        #'debug_toolbar.panels.signals.SignalDebugPanel',
        #'debug_toolbar.panels.logger.LoggingPanel',
    #)

    #DEBUG_TOOLBAR_CONFIG = {
        #'INTERCEPT_REDIRECTS': False,
    #}
